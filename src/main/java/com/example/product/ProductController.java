package com.example.product;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.web.bind.annotation.*;

import java.net.ConnectException;
import java.util.List;

@RestController
@RequestMapping("/product")
@RequiredArgsConstructor
@Slf4j
public class ProductController {
    private final ProductService productService;

    @GetMapping("/updateStock")
    @CacheEvict(value = "product", key = "#id")
    public Product updateStock(@RequestParam String id, @RequestParam int quantity) {
        log.info("cache miss");
        log.info("path=/product/updateStock");
        log.info("Updating stock: id={}, quantity={}", id, quantity);
        return productService.updateStock(id, quantity);
    }

    @PostMapping("/save")
    @CacheEvict(value = "products", allEntries = true)
    public Product saveProduct(@RequestBody Product product) {
        log.info("path=/product/save");
        log.info("Saving product: product={}", product);
        return productService.saveProduct(product);
    }

    @GetMapping("/get/{id}")
    @Cacheable(value = "product", key = "#id")
    public Product getProduct(@PathVariable String id) {
        log.info("path=/product/get/{id}");
        log.info("Getting product: id={}", id);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return productService.getProduct(id);
    }

    @GetMapping("/getAll")
    @Cacheable(value = "products")
    public List<Product> getProducts() {
        log.info("path=/product/getAll");
        log.info("Getting all products");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return productService.getProducts();
    }

    @DeleteMapping("/delete/{id}")
    @Caching(evict = {
            @CacheEvict(value = "product", key = "#id"),
            @CacheEvict(value = "products", allEntries = true)
    })
    public void deleteProduct(@PathVariable String id) {
        log.info("path=/product/delete/{id}");
        log.info("Deleting product: id={}", id);
        productService.deleteProduct(id);
    }

    @PutMapping("/update")
    @Caching(evict = {
            @CacheEvict(value = "product", key = "#product.id"),
            @CacheEvict(value = "products", allEntries = true)
    })
    public Product updateProduct(@RequestBody Product product) {
        log.info("path=/product/update");
        log.info("Updating product: product={}", product);
        return productService.saveProduct(product);
    }
}
