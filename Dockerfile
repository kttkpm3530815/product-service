FROM openjdk:17-alpine
# Set the working directory in the container
WORKDIR /app
# Copy the executable JAR into the container
COPY target/*.jar ./product-service.jar
# Set environment variables
#ENV REDIS_HOST=redis
#ENV MONGO_HOST=mongo
# Expose port 8080 to the outside world
EXPOSE 8081
CMD ["java", "-jar", "product-service.jar"]